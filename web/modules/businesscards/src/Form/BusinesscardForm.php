<?php

namespace Drupal\businesscards\Form;

use Drupal\businesscards\Formservice;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a businesscards form.
 */
class BusinesscardForm extends FormBase
{

  protected $formService;

  public static function create(ContainerInterface $container)
  {
    return new static($container->get('businesscards.formservice'));
  }


  public function __construct(Formservice $formservice)
  {
    $this->formService = $formservice;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'businesscards_businesscard';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state,$id=null)
  {
    if(!is_null($id)){
        $this->formService->getRecordById($id);
        $form_state->set('id',$id);
    }

    $form['messages'] = [
      '#type' => 'table',
      "#header"=>[
        $this->t('Name'),
        $this->t('Email'),
        $this->t('Phone'),
        $this->t('Description'),
      ],
      "#empty"=>$this->t('Table is empty'),
      '#rows' => $this->formService->getRows()
    ];


    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      "#required" => true,
      '#attributes' => array('placeholder' => t('John Doe')),
      '#default_value'=>$this->formService->get('name'),
    ];

    $form['email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email'),
      '#attributes' => array('placeholder' => t('example@example.com')),
      '#default_value'=>$this->formService->get('email'),
    ];

    $form['tel'] = [
      '#type' => 'tel',
      '#title' => $this->t('Phone'),
      '#attributes' => array('placeholder' => t('+36304620915')),
      '#default_value'=>$this->formService->get('tel'),
    ];

    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message'),
      '#default_value'=>$this->formService->get('message'),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
    ];

    if ($id !== null) {
      $form['actions']['delete'] = [
        '#type' => 'submit',
        '#value' => $this->t('Delete'),
        '#submit' => ['::deleteBusinesscard'],
      ];

    }


    return $form;
  }

  public function deleteBusinesscard(array &$form,FormStateInterface $form_state){
    $this->formService->delete($form_state->get('id'));
    $form_state->setRedirect('businesscards.businesscard');
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    if (mb_strlen($form_state->getValue('name')) < 5 || mb_strlen($form_state->getValue('name')) > 50) {
      $form_state->setErrorByName('name', $this->t('A név mező hossza nem megfelelő. Minimum 5 de maximum csak 50 karakter lehet'));
    }

    if (!filter_var($form_state->getValue('email'), FILTER_VALIDATE_EMAIL)) {
      $form_state->setErrorByName('email', $this->t('Nem megfelelő az email cím formátuma'));
    }

    if (substr($form_state->getValue('tel'), 0, 3) != '+36') {
      $form_state->setErrorByName('tel', $this->t('Nem megfelelő telefonszám formátum. Fontos hogy +36 kezdődjön'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $id = $form_state->get('id');
    if(empty($id)){
        $this->formService->save($form_state);
    }else{
      $this->formService->update($id,$form_state);
    }
    $this->messenger()->addStatus($this->t('Sikeres mentés'));
    $form_state->setRedirect('businesscards.businesscard');
  }

}
