<?php

namespace Drupal\businesscards;

use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;

/**
 * Formservice service.
 */
class Formservice {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  protected $table="businesscards";
  protected $fields=[
    'name','email','tel','message'
  ];
  protected $record;

  /**
   * Constructs a Formservice object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }


  protected function getTableFieldsDataFromFormStateObject(FormStateInterface $formState){
    return array_intersect_key($formState->getValues(),array_flip($this->fields));
  }

  public function save(FormStateInterface $formState){
    $this->connection->insert($this->table)
      ->fields($this->getTableFieldsDataFromFormStateObject($formState))
      ->execute();
  }

  public function update($id,FormStateInterface $formState){
    $this->connection->update($this->table)
      ->fields($this->getTableFieldsDataFromFormStateObject($formState))
      ->condition('id',$id)
      ->execute();
  }

  public function delete($id){
      $this->connection->delete($this->table)
        ->condition('id',$id)
        ->execute();
  }

  protected function getAlias(){
    return substr($this->table,0,1);
  }

  public function has($field){
    return (!empty($this->record))?array_key_exists($field,$this->record):null;
  }

  public function get($field){
    return $this->has($field)?$this->record[$field]:null;
  }

  public function getRecordById($id){
    $this->record=$this->connection->select($this->table,$this->getAlias())
      ->fields($this->getAlias(),$this->fields)
      ->condition('id',$id)
      ->execute()
      ->fetchAssoc();

    return $this;
  }

  public function getRows() {
    $result = $this->connection->select($this->table, $this->getAlias())
      ->fields($this->getAlias(), array_merge(['id'],$this->fields))
      ->execute();
    $rows = [];
    foreach($result as $row) {
      $row->name = Link::createFromRoute($row->name, 'businesscards.businesscard.edit', ['id' => $row->id]);
      $rows[] = array_intersect_key((array)$row,array_flip($this->fields));
    }
    return $rows;
  }


}
